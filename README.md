# Base Image build-debian 
Image zum erstellen von Debian Images

## Imagedetails
### User:
- root

### Port:
- keiner

### zusätzlich installierte pakete: 
- keine

### Umgebungsvariablen:
   
### Volumes:
- keine

### Build:
 - podman, da Volume mount möglich
 - capabilities erforgerlich mindestend CAP_MKNOD
 - Beispiel: sudo podman build -v /var/tmp/strap_cache:/strap_cache --no-cache --cap-add=CAP_MKNOD --build-arg debcode=bullseye -t debian-multitest:bullseye-0.6
 - darauf basisende Images sollten apt proxy config mit podman mounten