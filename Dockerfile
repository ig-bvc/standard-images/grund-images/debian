FROM registry.opencode.de/ig-bvc/standard-images/grund-images/debian:bullseye AS build
ARG debcode
ENV DEBIANCODE=$debcode
ENV DEBIAN_FRONTEND=noninteractive
WORKDIR /build
COPY excludelist .
ARG HTTP_PROXY
ENV DEBIAN_FRONTEND=noninteractive
ADD --chown=root:root $DEBIANCODE.list /etc/apt/sources.list.d/security-$DEBIANCODE.list
RUN if ! [ -z HTTP_PROXY ]; then echo "Acquire::http { Proxy \"$HTTP_PROXY\"; };" > /etc/apt/apt.conf.d/02proxy; fi &&\
    if ! [ -z HTTP_PROXY ]; then echo "Acquire::https { Proxy \"$HTTP_PROXY\"; };" >> /etc/apt/apt.conf.d/02proxy; cat /etc/apt/apt.conf.d/02proxy; fi &&\
    apt-get update && apt-get install -y wget apt-transport-https gnupg lsb-release &&\
    wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | tee /usr/share/keyrings/trivy.gpg > /dev/null &&\
    echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | tee -a /etc/apt/sources.list.d/trivy.list &&\
    apt-get update && apt-get install -y debootstrap trivy &&\
    http_proxy=$HTTP_PROXY /usr/sbin/debootstrap --variant=minbase --include=apt-utils $DEBIANCODE rootfs &&\
    if ! [ -z HTTP_PROXY ]; then cp /etc/apt/apt.conf.d/02proxy /build/rootfs/etc/apt/apt.conf.d/02proxy; fi
ADD --chown=root:root $DEBIANCODE.list /build/rootfs/etc/apt/sources.list.d/security-$DEBIANCODE.list
ADD --chown=root:root apt_docker.conf /build/rootfs/etc/apt/apt.conf.d/docker.conf
RUN chroot rootfs apt-get update && chroot rootfs apt-get upgrade -y
RUN /bin/tar cf rootfs.tar -C rootfs -X excludelist .
RUN mkdir stage && cd stage && tar xf /build/rootfs.tar
RUN trivy fs --format json --output /build/stage/bom.json /build/stage/

FROM scratch
ENV DEBIAN_FRONTEND=noninteractive
COPY --from=build /build/stage/ /
CMD /bin/bash
